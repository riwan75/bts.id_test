import React from 'react';
import type {Node} from 'react';

// Navigation
import {NavigationContainer} from '@react-navigation/native';
// Screen
import RootStackScreen from './src/RootStack';

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default App;
