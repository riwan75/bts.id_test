import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  Image,
  Keyboard,
  Appearance,
} from 'react-native';
import axios from 'axios';
import CustomButton from '../components/common/CustomButton';
import CustomTextInput from '../components/common/CustomTextInput';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Login = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      const {data} = await axios({
        method: 'POST',
        url: 'http://18.139.50.74:8080/login',
        data: {
          username,
          password,
        },
        headers: {
          'content-type': 'application/json',
        },
      });
      AsyncStorage.setItem('accessToken', data.data.token);
      navigation.navigate('FreeTimeQuestion');
    } catch (error) {
      if (error.response !== undefined) {
        if (error.response.status === 403) {
          alert('Your email or password is wrong');
        } else if (error.response.status === 401) {
          alert('Your email or password is wrong');
        } else if (error.response.status === 400) {
          alert('Your email or password is wrong');
        }
      } else {
        console.log(error);
        alert('Your email or password is wrong');
      }
    }
  };

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <View style={{alignItems: 'center', marginVertical: 70}}>
          {/* <Image
            style={styles.tinyLogo}
            source={
              Appearance.getColorScheme() === 'dark'
                ? require('../assets/isaylogoputih.png')
                : require('../assets/isaylogo.png')
            }
          /> */}
        </View>
        <Text style={styles.email}>Username</Text>
        <CustomTextInput
          placeholder="Username"
          value={username}
          onChangeText={setUsername}
        />
        <Text style={styles.password}>Password</Text>
        <CustomTextInput
          placeholder="Password"
          secureTextEntry
          value={password}
          onChangeText={setPassword}
        />
        <Text style={styles.forgotPassword}>Forgot Password?</Text>
        <View style={styles.container1}>
          <CustomButton title="Login" onPressButton={() => handleLogin()} />
          <View style={styles.signUp}>
            <Text>Haven't an account yet? </Text>
            <Text
              style={styles.signupColor}
              onPress={() => navigation.navigate('Register')}>
              Sign up here
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
  },
  email: {
    left: 20,
    marginBottom: 15,
    fontWeight: 'bold',
  },
  password: {
    left: 20,
    marginBottom: 15,
    marginTop: 30,
    fontWeight: 'bold',
  },
  forgotPassword: {
    left: 20,
    marginBottom: 15,
    marginTop: 0,
  },
  container1: {
    justifyContent: 'flex-end',
    flex: 1,
  },
  signUp: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 25,
    marginBottom: 25,
  },
  signupColor: {
    color: '#4781F8',
    fontWeight: 'bold',
  },
  tinyLogo: {
    width: 170,
    height: 70,
  },
});

//
