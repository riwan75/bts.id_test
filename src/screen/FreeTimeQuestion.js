import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  FlatList,
  SafeAreaView,
} from 'react-native';
import {color} from '../styles/color';
import axios from 'axios';
import CustomButton from '../components/common/CustomButton';
import CustomCheckbox from '../components/common/CustomCheckbox';
import AsyncStorage from '@react-native-async-storage/async-storage';

const FreeTimeQuestion = ({route, navigation}) => {
  const [activity, setActivity] = useState([]);
  const [isRefresh, setIsRefresh] = useState(false);
  const [activitySelected, setActivitySelected] = useState([]);

  useEffect(async () => {
    let url = 'http://18.139.50.74:8080/checklist';
    const token = await AsyncStorage.getItem('accessToken');
    const AuthStr = 'Bearer '.concat(token);
    try {
      const response = await axios.get(url, {
        headers: {Authorization: AuthStr, 'Content-Type': 'application/json'},
      });
      setActivity(response.data.data);
      console.log('test activity', response.data.data);
    } catch (error) {
      console.log('Error', error);
    }
  }, [activitySelected]);

  const interest = id => {
    let selectedActivity = activitySelected;

    if (selectedActivity.length == 0) {
      selectedActivity.push(id);
    } else {
      let idx = selectedActivity.findIndex(activityid => activityid == id);
      if (idx == -1) {
        selectedActivity.push(id);
      } else {
        selectedActivity.splice(idx, 1);
      }
    }
    // console.log('run', selectedActivity);
    setActivitySelected(activitySelected);
    setIsRefresh(!isRefresh);
  };

  const renderItem = ({item}) => {
    // console.log('item', item);

    return (
      <CustomCheckbox
        title={item.name}
        id={item.id}
        activitySelected={activitySelected}
        onPress={() => interest(item._id)}
      />
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View View style={styles.container}>
        <Text style={styles.welcome}>Choose item</Text>
        <View style={styles.container1}>
          <Text style={styles.text1}>Choose</Text>
        </View>

        <FlatList
          data={activity}
          renderItem={renderItem}
          keyExtractor={item => item._id}
        />
        <View style={styles.button}>
          {/* <CustomButton
            title="Next"
            onPressButton={() =>
              navigation.navigate('InterestTopic', {
                activitySelected,
              })
            }
          /> */}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default FreeTimeQuestion;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  container1: {
    alignItems: 'center',
    marginBottom: 20,
  },
  welcome: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 20,
    marginBottom: 20,
  },
  text1: {
    fontSize: 13,
    width: '70%',
    textAlign: 'center',
    color: color.black,
    marginVertical: 5,
  },
  button: {
    flex: 0.1,
    marginBottom: 30,
    justifyContent: 'flex-end',
  },
});
