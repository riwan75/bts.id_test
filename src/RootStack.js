import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../src/screen/Login';
import Register from '../src/screen/Register';
import FreeTimeQuestion from './screen/FreeTimeQuestion';

const Stack = createStackNavigator();

const RootStackScreen = ({navigation}) => (
  <Stack.Navigator headerMode="none" initialRouteName="Login">
    <Stack.Screen name="Login" component={Login} />
    <Stack.Screen name="Register" component={Register} />
    <Stack.Screen name="FreeTimeQuestion" component={FreeTimeQuestion} />
  </Stack.Navigator>
);

export default RootStackScreen;
